﻿using UnityEngine;
using System.Collections;

public class boatMovement : MonoBehaviour 
{
	//Modifiers
		//these will adjust the boats Speed
		//they will be altered by boat upgrades
	int linearSpeed;
	int turnSpeed;



	// Use this for initialization
	void Start () 
	{
		linearSpeed = 5;
		turnSpeed = 20;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//the boat can only turn when it is moving forward or back.
			//it cant turn on the spot
			//I think thats how boats work

		//move the boat forward when 'w' key is pressed
		if (Input.GetKey("w")) 
		{

			transform.Translate (Vector3.forward * Time.deltaTime * linearSpeed);

			//turn the boat to the right
			if (Input.GetKey("d")) 
			{
				transform.Rotate(Vector3.up, Time.deltaTime * turnSpeed);
			} 

			//turn the boat to the left
			else if (Input.GetKey("a")) 
			{

				transform.Rotate(Vector3.down, Time.deltaTime * turnSpeed);
			} 
		} 

		//move the boat backwards if the 's' key is pressed
		else if (Input.GetKey("s")) 
		{

			transform.Translate (Vector3.back * Time.deltaTime * linearSpeed);

			//turn the boat to the right
			if (Input.GetKey("d")) 
			{
				transform.Rotate(Vector3.down, Time.deltaTime * turnSpeed);
			} 

			//turn the boat to the left
			else if (Input.GetKey("a")) 
			{

				transform.Rotate(Vector3.up, Time.deltaTime * turnSpeed);
			} 
		} 
	


	//Modifier tests, to be rmoved
		if (Input.GetKeyDown ("up")) 
		{
			linearSpeed+=5;
		}
		if (Input.GetKeyDown ("down")) 
		{
			linearSpeed-=5;
		}
		if (Input.GetKeyDown ("right")) 
		{
			turnSpeed+=20;
		}
		if (Input.GetKeyDown ("left")) 
		{
			turnSpeed-=20;
		}

	}
}
