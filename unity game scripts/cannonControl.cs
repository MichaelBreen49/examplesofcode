﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


//
//cannon controller script
//	Purpose is to fire all relevantcannons on mousepress
//	at cannon creation they wil be added to relevant array
//	
//
public class cannonControl : MonoBehaviour {



	//public GameObject test;
	public GameObject CannonBall;
	public List<GameObject> portCannons;
	public List<GameObject> starboardCannons;
	// Use this for initialization
	void Start () 
	{
		portCannons = new List<GameObject>();
		starboardCannons = new List<GameObject>();

	}
	
	// Update is called once per frame
	void Update () 
	{

		//Fire cannons
			//left click fires port cannons
		if (Input.GetMouseButtonDown (0)) 
		{
			foreach(GameObject cannon in portCannons)
			{
				Instantiate(CannonBall, new Vector3(cannon.transform.position.x, cannon.transform.position.y, cannon.transform.position.z), (cannon.transform.rotation* Quaternion.AngleAxis(-90,Vector3.up)));

			}
		}

		//Fire cannons
		//right click fires starboard cannons
		if (Input.GetMouseButtonDown (1)) 
		{
			foreach(GameObject cannon in starboardCannons)
			{
				Instantiate(CannonBall, new Vector3(cannon.transform.position.x, cannon.transform.position.y, cannon.transform.position.z), (cannon.transform.rotation* Quaternion.AngleAxis(-90,Vector3.up)));
				//print(cannon.name);
			}
		}
	}
	
	//
	//add cannons correct array by finding which way it is pointing
	//
	public void addCannon(GameObject x)
	{
		print(x.name);
		//print (x.name+" : "+ x.transform.InverseTransformPoint(transform.position) + "    Boat rotation.y : "+transform.position);
		if (x.transform.InverseTransformPoint (transform.position).z < transform.position.z)
		{	
			portCannons.Add (x);
		}
		else
			starboardCannons.Add (x);
	}


}
