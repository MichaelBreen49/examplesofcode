

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JLabel;







public class CRUDGui extends JFrame implements ActionListener
{
	//the various bits I included

	JButton create = new JButton("Create");
	JButton read = new JButton("Read");
	JButton update = new JButton("Update");
	JButton delete = new JButton("Delete");
	//JTable jTable = new JTable();
	JPanel panel1 = new JPanel();
	JPanel panel2 = new JPanel();
	Vector<String> columnNames=new Vector<String>();
	
	Vector<Vector>rowData=new Vector<Vector>();
	
	private String theURL = "rmi:///";
	private Server theFactory;
	
	
	//private CpuFactory theFactory;
	
	JLabel label = new JLabel();
	

	
	
	
	CRUDGui(String a) 
	{ 
		
		//
		//sacrifice given to super class
		//
		super(a);
		
		
	    try 
	    {
			theFactory  = (Server)Naming.lookup(theURL+"factory");
		} 
	    catch (MalformedURLException e) 
	    {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	    catch (RemoteException e) 
	    {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			validateWorking();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//
	//what the buttons do
	//
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==create)
		{
			CreateCpuWindow a  = new CreateCpuWindow("Create cpu",this);
			setVisible(false);
		}
		if(e.getSource()==read)
		{
		//	ReadCpuWindow a = new ReadCpuWindow("Read cpu",this);
		}
		if(e.getSource()==update)
		{
			UpdateCpuWindow a  = new UpdateCpuWindow("Updat cpu",this);
			setVisible(false);
		}
		if(e.getSource()==delete)
		{
			DeleteCpuWindow a  = new DeleteCpuWindow("Delete cpu",this);
			setVisible(false);
		}
		//validateWorking();
		
	}
	
	public Vector<Vector> getRowData() throws RemoteException
	{
		Vector<Cpu>data1=theFactory.getListOf();
		Vector<Vector> rowData1=new Vector<Vector>();
		for(int i=0;i<data1.size();i++)
		{
			Vector v = new Vector();
			v.add(""+i);
			v.add(data1.get(i).getName());
			v.add(""+data1.get(i).getRating());
			v.add(""+data1.get(i).getRating());
			v.add(""+data1.get(i).getPrice());
			v.add(""+data1.get(i).getClockSpeed());
			v.add(""+data1.get(i).getCores());
			v.add(""+data1.get(i).getCache());
			
			rowData1.add(v);
		}
		
		return rowData1;
		
	}
	
	public void validateWorking() throws RemoteException
	{
		//-------------------------------------------------------------------------------------fucked from here
		panel2.removeAll();
		panel1.removeAll();
		//rowData.clear();
		columnNames.clear();
		
		getContentPane().removeAll();
		
		
		
		getContentPane().setLayout(new GridLayout(2,1));

		panel1.setLayout(new GridLayout(2,2));
		
		//add the actionlistener to the buttons
		create.addActionListener(this);
		read.addActionListener(this);
		update.addActionListener(this);
		delete.addActionListener(this);
		
		//add the leftPanel in the format of borderLayout WEST to the contentPane
		panel1.add(create);
		//add the rightPanel in the format of borderLayout CENTER to the contentPane
		panel1.add(read);
		panel1.add(update);
		panel1.add(delete);
		
		//---------------------------------------------------------------------------------------------------------------TableStart
		columnNames.add("ID");
		columnNames.add("Name");
		columnNames.add("Price");
		columnNames.add("Rating");
		columnNames.add("Clock");
		columnNames.add("Cores");
		columnNames.add("Cache");
		
		
		//-----------
		
		
		//rowData.add(data);
		
		JTable jTable=new JTable(getRowData(),columnNames );
		jTable.setPreferredScrollableViewportSize(new Dimension(500, 70));
		jTable.setFillsViewportHeight(true);
		
		JScrollPane scrollPane = new JScrollPane(jTable);
		panel2.add(scrollPane);
		
		//panel2.add(jTable);
		//---------------------------------------------------------------------------------------------------------------TableFinish
		
		getContentPane().add(panel2);
		getContentPane().add(panel1);
		//set the size 
		setSize(540,250);
		

		//remembered this this time so huzzah
		setVisible(true);
	}
	
		
		
	
	public static void main(String[]args) throws ClassNotFoundException, IOException, NotBoundException
	{
		new CRUDGui("cpu stuff");
	}
	
	
}
