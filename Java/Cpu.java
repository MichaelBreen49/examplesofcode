

import java.io.Serializable;

public class Cpu implements Serializable
{
//private
	//various variables used by cpu
	private String name;
	private int rating;
	private int price;
	private double clockSpeed;
	private int cores;
	private int cache;
	
	
//public
	//
	//Constructor sets various values
	//
	Cpu(String nameParam,int ratingParam, int priceParam, double clockParam, int coresParam, int cacheParam)
	{
		name = nameParam;
		rating = ratingParam;
		price = priceParam;
		clockSpeed = clockParam;
		cores = coresParam; 
		cache = cacheParam;
	}
	
	//
	//getters and setters for all instance variables
	//
	public String getName() 
	{
		return name;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
	public int getRating() 
	{
		return rating;
	}
	public void setRating(int rating) 
	{
		this.rating = rating;
	}
	public int getPrice() 
	{
		return price;
	}
	public void setPrice(int price) 
	{
		this.price = price;
	}
	public double getClockSpeed() 
	{
		return clockSpeed;
	}
	public void setClockSpeed(double clockSpeed) 
	{
		this.clockSpeed = clockSpeed;
	}
	public int getCores() 
	{
		return cores;
	}
	public void setCores(int cores) 
	{
		this.cores = cores;
	}
	public int getCache() 
	{
		return cache;
	}
	public void setCache(int cache) 
	{
		this.cache = cache;
	}
}
