

#include "Admin.h"
#include "Guest.h"
#include <vector>
#include "Li.h"

void main()
{
//setup
	//some program variables used to keep program going
		//---------------------------------------
	int choice;
	bool keepGoing =true;
	bool coolStory=true;
		//---------------------------------------
	//The group of users used for logins
	vector <User*> users;
	
	vector<int>inttest;

	//some initial data for my cool book repository
	 Book* test= new Book("Test","Test",0);

//List
	//in my linked list start point is crucial
	//startpoint
	Li<Book>* bookList =new Li <Book> (test);
	//insert using startpoint
	bookList->add(bookList,(new Li <Book>(new Book("ToKillAMockingBird","HarperLee",100))));
	bookList->add(bookList,(new Li <Book>(new Book("LordOfTheRings","Tolkien",200))));

//User
	//user* used as current login
	User* user;
	//add Guest and admin from start
	users.push_back(new Admin());
	//------------------------------------------------------------test
	//------------------------------------------------------------fintest
	users.push_back(new Guest("Guest","Guest"));
	//cout<<"\n------------------------------"<<&users[0]<<"-----------------------------";
//end of setup

//program run
	while(coolStory)
	{
		//password control resets to "" after each iteration of loop
		string coolPasswordChecker="";
		//used for second loop
		keepGoing=true;
		//select user from vector of users
		cout<<"Who would you like to login as?\n\t";
		for(int i=0;i<users.size();i++)
		{
			cout<<i<<")"<<users[i]->getName()<<"\n\t";
		}

		cout<<"-1)Quit\n:::";
		cin>>choice;
		if(choice==-1)
		{
			coolStory=false;
		}
		else
		{
			user = users[choice];
			//password verification
				//not needed for guest
			if(user->getPassword()!="Guest")
			{
				cout<<"\tWhat is the password for that user?\n\t\t:::";
				cin>>coolPasswordChecker;
				if(coolPasswordChecker!=user->getPassword())
				{
					cout<<"nope thats not the password\n\n";
					user=NULL;
					keepGoing=false;
				}
			}
			//so user has now been selected
			while(keepGoing)
			{
				//Print out various options depending on login type
				int input=0;	
				cout<<user->getType()<<"\n\t";
				if(user->getType()=="Admin")
				{
					cout<<"what would you like to do?\n\t\t1)"<< user->get1()<<"\n\t\t2)"<< user->get2()<<"\n\t\t3)"<< user->get3()<<"\n\t\t4)Add registered user?\n\t\t5)Remove registered user"<<"\n\t\t6)Logout\n\t\t:::";
					cin>>input;
				}
				else if(user->getType()=="Guest")
				{
					cout<<"what would you like to do?\n\t\t1)"<< user->get1()<<"\n\t\t6)Logout\n\t\t:::";
					cin>>input;
				}
				else
				{
					cout<<"what would you like to do?\n\t\t1)"<< user->get1()<<"\n\t\t2)"<<user->get2()<<"\n\t\t3)"<< user->get3()<<"\n\t\t6)Logout\n\t\t:::";
					cin>>input;
				}
				cout<<"\n";

				//Take input and act accordingly
				if(input==1)
				{
					user->one(bookList);
				}
				if(input==2)
				{
					user->two(bookList);
				}
				if(input==3)
				{
					user->three(bookList);
				}
				if(input==4)
				{
					user->four(&users);
				}
				if(input==5)
				{
					user->five(&users);
				}
				if(input==6)
				{
					keepGoing=false;
				}
		
			}
		}
	}
	//
	//delete books
	//
/*	Li<Book>* llPtr = bookList;
	while(llPtr->getNext()!=NULL)
	{
		llPtr=llPtr->getNext();
		delete llPtr->getPrev()->getData();
		//llPtr=llPtr->getPrev();
		
	}
	//
	//CANT REMEMBER-----------------------------------------------FIX
	/*
	for(int i=0;i<users.size();i++)
	{
		delete   users[i];

	}*/
	system("pause");
}