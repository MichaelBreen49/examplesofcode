#include <iostream>
#include <string>
using namespace std;

#ifndef Li_H
#define Li_H

//
//LinkedListClass
//
template <class T>
class Li
{
private:
	//the data contained such as book
	T* data;
	//the first item in the list useful for quick selects
	Li* first;
	//the last item int the list useful for quick selects
	Li* last;
	//the next item that is linked
	Li* next;
	//the previously linked item
	Li* prev;
public:
	//
	//the cool constructor sets the variables to different things
	//
	Li(T* dataP)
	{
		first=NULL;
		last=NULL;
		next=NULL;
		prev=NULL;

		data = dataP;
	}

	//
	//returns the data... such as book
	//
	T* getData()
	{
		return data;
	}

	//
	//sets the data
	//
	void setData(T*  dataParam)
	{
		data=dataParam;
	}
	//
	//various other getters and setters
	//
	Li* getFirst()
	{
		return first;
	}
	Li* getLast()
	{
		return last;
	}
	Li* getNext()
	{
		return next;
	}
	Li* getPrev()
	{
		return prev;
	}

	void setFirst(Li* a)
	{
		first=a;
	}
	void setLast(Li* a)
	{
		last=a;
	}
	void setNext(Li* a)
	{
		next=a;
	}
	void setPrev(Li* a)
	{
		prev=a;
	}
	//
	//add an element to coolList will be added infront of atPosition
	//		handles the next and previous pointer changes
	//
	template <class T>
	void add(T* atPosition, T* newBook)
	{
		if(atPosition->getNext()!=NULL)
			(atPosition->getNext())->setPrev(newBook);
		newBook->setNext(atPosition->getNext());
		newBook->setPrev(atPosition);
		atPosition->setNext(newBook);
	}

	//
	//removes an element  from  coolList 
	//		handles the next and previous pointer changes
	//
	template <class T>
	void remove(T* toRemove)
	{
		if(toRemove->getPrev()!=NULL)
			toRemove->getPrev()->setNext(toRemove->getNext());
		else
			toRemove->getNext()->setPrev(NULL);
		if(toRemove->getNext()!=NULL)
			toRemove->getNext()->setPrev(toRemove->getPrev());
		else
			toRemove->getPrev()->setNext(NULL);
		toRemove=NULL;
	}


	
};

#endif