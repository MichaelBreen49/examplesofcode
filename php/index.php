<?php

$db = mysqli_connect('localhost', 'root', '') or
                die ('Unable to connect. Check your connection parameters boobboob.');
                mysqli_select_db($db, 'assignment2') or die(mysqli_error($db));
//------------------------------------------------------------------StartProcedure
                //addUser procedure -------------
                    //drop if exists
                $query2 = "drop procedure if exists adduser;";
                mysqli_query($db, $query2) or die(mysqli_error($db));
                    //procedure to insert data into userInfo table
                $query3 = "create procedure addUser".
                    "(in email varchar(20), password varchar(20), firstname varchar(20), lastname varchar(20))".
                    "begin insert into userinfo (emailaddress, password, firstname, lastname, priviledge)".
                    "values(email, password, firstname, lastname, 'normal'); end;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //-------------------------------
                
                //select userID that corresponds to inputted emailadress
                    //drop if already exists
                $query2 = "drop procedure if exists simpleUserIdFromEmail;";
                mysqli_query($db, $query2) or die(mysqli_error($db));
                    //select statement in procedure form
                $query2 = "create procedure simpleUserIdFromEmail".
                    " (in email varchar(20)) begin select userId from userinfo where emailaddress = email ; end;";
                    
                mysqli_query($db, $query2) or die(mysqli_error($db));
                //-------------------------------
       
                //select Userid from userinfo where password = '".$password."';
                    //drop procedure if exists
                $query3 = "drop procedure if exists getPassword;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //select statement in procedure form
                $query3 = "create procedure getPassword".
                    " (in pass varchar(20)) begin select userId from userinfo where password = pass ; end;";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //-------------------------------
             //get the priviledge associated with a certain user
                    //drop procedure if exists
                $query3 = "drop procedure if exists getPriviledge;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //select statement in procedure form
                $query3 = "create procedure getPriviledge".
                    " (in user int) begin select priviledge from userinfo where userId = user ; end;";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
                
               //add a movie to watched for a certain user
                    //drop procedure if exists
                $query3 = "drop procedure if exists getMovie;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //procedure to set the movieWatchlist value to 1 which will mean the user has watched it
                $query3 = "create procedure getMovie".
                    " (in user int, in movie varchar(50))"
                    . "begin update movieUsers set userWatchlist=1 where movieId = (select movieId from infoMovie where moviename = movie)and userId = user; end";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
                
                //delete movie from list
                    //drop procedure if exists
                $query3 = "drop procedure if exists delMovie;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //select statement in procedure form
                $query3 = "create procedure delMovie".
                    " (in user int, in movie varchar(50))"
                    . "begin delete from movieUsers where userId = user and movieId = (select movieid from infomovie where moviename = movie); end";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
                //rate movies that user has watched
                    //drop procedure if exists
                $query3 = "drop procedure if exists rateMovie;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //select statement in procedure form
                $query3 = "create procedure rateMovie".
                    " (in user int, in movie varchar(50), in rating int)"
                    . "begin update movieUsers set userrating = rating where movieId = (select movieId from infoMovie where moviename = movie) and userId = user; end";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
                
                //select movies that user has watched
                    //drop procedure if exists
                $query3 = "drop procedure if exists unwatchedMovie;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //select statement in procedure form
                $query3 = "create procedure unwatchedMovie".
                    " (in user int)"
                    . "begin SELECT infoMovie.moviename from infoMovie, movieusers where infoMovie.movieId = movieUsers.movieId and movieUsers.userid = user and movieUsers.userWatchlist = 1; end";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
                
                //select movies that user hasn't watched
                    //drop procedure if exists
                $query3 = "drop procedure if exists watchedMovie;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //select statement in procedure form
                $query3 = "create procedure watchedMovie".
                    " (in user int)"
                    . "begin SELECT infoMovie.moviename from infoMovie, movieusers where infoMovie.movieId = movieUsers.movieId and movieUsers.userid = user and movieUsers.userWatchlist = 0; end";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
                 
                //add a movie to a users watchlist
                    //drop procedure if exists
                $query3 = "drop procedure if exists addToWatch;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //select statement in procedure form
                $query3 = "create procedure addToWatch".
                    " (in user int, in movie varchar(50))"
                    . "begin insert into movieUsers (userId, movieId, userRating, Userwatchlist) values (user, (select movieId from infoMovie where movieName = movie),1,0); end";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
                
                //rate movies that user has watched
                    //drop procedure if exists
                $query3 = "drop procedure if exists getGenreMovies;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //select statement in procedure form
                $query3 = "create procedure getGenreMovies".
                    " (in genre varchar(20))"
                    . "begin SELECT moviename from infoMovie where  movieGenre= genre ; end";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
            
                //rlist top rated movies
                    //drop procedure if exists
                $query3 = "drop procedure if exists listTopRated;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //get the movie name and rating for the top rated movies
                $query3 = "create procedure listTopRated".
                    " (in genre varchar(20))"
                    . "begin select moviename, sum(userrating) from infomovie, movieusers where infomovie.movieid = movieusers.movieid group by movieusers.movieid order by userrating desc limit 5;  end";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
                
                //list the bottom rated movies
                    //drop procedure if exists
                $query3 = "drop procedure if exists listBotRated;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //get the movie names and rating for the bottom rated movies
                $query3 = "create procedure listBotRated".
                    " (in genre varchar(20))"
                    . "begin select moviename, sum(userrating) from infomovie, movieusers where infomovie.movieid = movieusers.movieid group by movieusers.movieid order by userrating asc limit 5;  end";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
                
                //list the most watched movie
                    //drop procedure if exists
                $query3 = "drop procedure if exists listMostWatched;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                     //Sum up the UserWatchlist (as 0 = not watched and 1 = watched) values and group by movie
                $query3 = "create procedure listMostWatched".
                    " (in genre varchar(20))"
                    . "begin select moviename, sum(UserWatchlist) from infoMovie, movieUsers where infomovie.movieId = movieUsers.movieId group by movieusers.movieid order by sum(UserWatchlist) desc limit 5;  end";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
                
                //list the least watched movie that people want to watch
                    //drop procedure if exists
                $query3 = "drop procedure if exists listLeastWatched;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //Sum up the UserWatchlist (as 0 = not watched and 1 = watched) values and group by movie
                $query3 = "create procedure listLeastWatched".
                    " (in genre varchar(20))"
                    . "begin select moviename, sum(UserWatchlist) from infoMovie, movieUsers where infomovie.movieId = movieUsers.movieId group by movieusers.movieid order by sum(UserWatchlist) asc limit 5;  end";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
                
                
                //Select all non admin users
                    //drop procedure if exists
                $query3 = "drop procedure if exists listNormies;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //Select all users whose priviledge != admiin
                $query3 = "create procedure listNormies".
                    " (in genre varchar(20))"
                    . "begin select firstname from userinfo where priviledge != 'admin';  end";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
                
                //Make an admin
                    //drop procedure if exists
                $query3 = "drop procedure if exists makeAdmin;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //Select all users whose priviledge != admiin
                $query3 = "create procedure makeAdmin".
                    " (in user varchar(20))"
                    . "begin update userinfo set priviledge = 'admin' where firstname = user ;  end";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
                
                //list the most active users
                    //drop procedure if exists
                $query3 = "drop procedure if exists mostActive;";
                mysqli_query($db, $query3) or die(mysqli_error($db));
                    //count the number of enteries for each user order by highest
                $query3 = "create procedure mostActive".
                    " (in genre varchar(20))"
                    . "begin select firstname, count(movieId) from userInfo, movieUsers where userinfo.userId = movieUsers.userId group by movieUsers.userId order by count(movieId) desc limit 5;  end";
                    
                mysqli_query($db, $query3) or die(mysqli_error($db));
                //------------------------------
             header('Location:index1.php');
?>